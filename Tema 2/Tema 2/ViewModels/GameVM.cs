﻿using Tema_2.Models;
using Tema_2.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Tema_2.ViewModels
{
    class GameVM
    {
        private GameBusinessLogic bl;
        public GameVM()
        {
            //if (Application.Current.MainWindow)
            //{
                ObservableCollection<ObservableCollection<Cell>> board = Helper.InitGameBoard(false);
                bl = new GameBusinessLogic(board);
                GameBoard = CellBoardToCellVMBoard(board);
            //}
        }

        public GameVM(ObservableCollection<ObservableCollection<Cell>> board)
        {
            bl = new GameBusinessLogic(board);
            GameBoard = CellBoardToCellVMBoard(board);
        }

        private ObservableCollection<ObservableCollection<CellVM>> CellBoardToCellVMBoard(ObservableCollection<ObservableCollection<Cell>> board)
        {
            ObservableCollection<ObservableCollection<CellVM>> result = new ObservableCollection<ObservableCollection<CellVM>>();
            for (int i = 0; i < board.Count; i++)
            {
                ObservableCollection<CellVM> line = new ObservableCollection<CellVM>();
                for (int j = 0; j < board[i].Count; j++)
                {
                    Cell c = board[i][j];
                    CellVM cellVM = new CellVM(c.X, c.Y, c.HidenImage, c.DisplayedImage, bl);
                    line.Add(cellVM);
                }
                result.Add(line);
            }
            return result;
        }

        public void RedrawBoard()
        {
            GameBoard = CellBoardToCellVMBoard(Helper.RedrawGameBoard(bl.cells)); 
        }

        public ObservableCollection<ObservableCollection<CellVM>> GameBoard { get; set; }
    }
}
