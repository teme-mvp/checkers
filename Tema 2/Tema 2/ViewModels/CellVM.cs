﻿using Tema_2.Commands;
using Tema_2.Models;
using Tema_2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Controls;

namespace Tema_2.ViewModels
{
    class CellVM : BaseNotification
    {
        GameBusinessLogic bl;
        public CellVM(int x, int y, string hidden, string displayed, GameBusinessLogic bl)
        {
            SimpleCell = new Cell(x, y, hidden, displayed);
            this.bl = bl;
        }

        private Cell simpleCell;
        public Cell SimpleCell
        {
            get { return simpleCell; }
            set
            {
                simpleCell = value;
                NotifyPropertyChanged("SimpleCell");
            }
        }

        private Button simpleBtn;
        public Button SimpleBtn
        {
            get { return simpleBtn; }
            set
            {
                simpleBtn = value;
                NotifyPropertyChanged("SimpleBtn");
            }
        }

        private ICommand clickCommand;
        public ICommand ClickCommand
        {
            get
            {
                if (clickCommand == null)
                {
                    clickCommand = new RelayCommand<Cell>(bl.ClickAction);

                }
                return clickCommand;
            }
        }

        /*private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand<Button>(bl.SaveConfig);

                }
                return saveCommand;
            }
        }*/
    }
}
