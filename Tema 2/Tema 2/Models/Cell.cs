﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Tema_2.Commands;

namespace Tema_2.Models
{

    class Cell : INotifyPropertyChanged
    {
        public Cell(int x, int y, string hidden, string displayed)
        {
            this.X = x;
            this.Y = y;
            this.HidenImage = displayed;
            this.DisplayedImage = displayed;
            this.isSelected = false;
        }

        private int x;
        public int X
        {
            get { return x; }
            set
            {
                x = value;
                NotifyPropertyChanged("X");
            }
        }
        private int y;
        public int Y
        {
            get { return y; }
            set
            {
                y = value;
                NotifyPropertyChanged("Y");
            }
        }
        private string hidenImage;
        public string HidenImage
        {
            get { return hidenImage; }
            set
            {
                hidenImage = value;
                NotifyPropertyChanged("HidenImage");
            }
        }
        private string displayedImage;
        public string DisplayedImage
        {
            get { return displayedImage; }
            set
            {
                displayedImage = value;
                NotifyPropertyChanged("DisplayedImage");
            }
        }

        private bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                NotifyPropertyChanged("IsSelected");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
