﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2.Models
{
    class Stats
    {
        public int playerOneWins { get; set; }
        public int playerTwoWins { get; set; }
        public Stats()
        {
            string[] lines = System.IO.File.ReadAllLines("D:/Facultate Alin/Anul II/Sem II/Medii Vizuale de Programare/Teme/Tema 2/Tema 2/Tema 2/Resources/stats.txt");
            playerOneWins = Int32.Parse(lines[0]);
            playerTwoWins = Int32.Parse(lines[1]);
        }
        
        public void addWin(int player)
        {
            if(player == 1)
            {
                playerOneWins++;
                writeWins();
            }
            else
            {
                playerTwoWins++;
                writeWins();
            }
        }

        public void showStats()
        {
            System.Windows.MessageBox.Show("Player 1 (BLACK) wins: " + playerOneWins + "\nPlayer 2 (RED) wins: " + playerTwoWins);
        }

        public void writeWins()
        {
            try
            {
                using (StreamWriter file = new StreamWriter("D:/Facultate Alin/Anul II/Sem II/Medii Vizuale de Programare/Teme/Tema 2/Tema 2/Tema 2/Resources/stats.txt"))
                {
                    file.WriteLine(playerOneWins);
                    file.WriteLine(playerTwoWins);
                    file.Flush();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error: ", ex);
            }
        }
    }
}
