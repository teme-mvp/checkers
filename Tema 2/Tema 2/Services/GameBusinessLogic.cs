﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Tema_2.Models;
using Tema_2.ViewModels;
using Tema_2.Views;
using System.Windows;
using System.Windows.Controls;
using System.IO;

namespace Tema_2.Services
{
    class GameBusinessLogic
    {
        public ObservableCollection<ObservableCollection<Cell>> cells;
        private bool turn;
        private int clickCounter;
        private int redPiecesErased;
        private int blackPiecesErased;
        private int numberOfInvalidMoves;
        private bool erasePiece;

        public GameBusinessLogic(ObservableCollection<ObservableCollection<Cell>> cells)
        {
            this.cells = cells;
            turn = false;
            clickCounter = 0;
            redPiecesErased = 0;
            blackPiecesErased = 0;
            numberOfInvalidMoves = 0;
            erasePiece = false;
        }

        private void SelectPiece(Cell cell)
        {
            if ((turn == false && (cell.HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg" || cell.HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg")) || (turn == true && (cell.HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg" || cell.HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg")))
            {
                Helper.CurrentCell = cell;
                if (cell.DisplayedImage != "/Tema 2;component/Resources/Cells/lightCell.jpg" && cell.DisplayedImage != "/Tema 2;component/Resources/Cells/darkCell.jpg")
                {
                    cell.IsSelected = true;
                    cell.DisplayedImage = "/Tema 2;component/Resources/Cells/selectedPiece.jpg";
                }
            }
            else
                clickCounter--;
        }

        public void Stay()
        {
            Helper.CurrentCell.DisplayedImage = Helper.CurrentCell.HidenImage;
            Helper.CurrentCell.IsSelected = false;
            numberOfInvalidMoves = 0;
            clickCounter--;
        }

        public int CanTakeOut(Cell current)
        {
            if(current.HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg" || current.HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg")
            {
                if(current.HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg")
                {
                    if(current.X > 1 && current.Y > 1)
                    {
                        if(cells[current.X - 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg" || cells[current.X - 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                        {
                            if(cells[current.X - 2][current.Y - 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return -1;
                            }
                        }
                    }
                    if(current.X > 1 && current.Y < 6)
                    {
                        if (cells[current.X - 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg" || cells[current.X - 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                        {
                            if (cells[current.X - 2][current.Y + 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return 1;
                            }
                        }
                    }
                    if(current.X < 6 && current.Y > 1)
                    {
                        if (cells[current.X + 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg" || cells[current.X + 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                        {
                            if (cells[current.X + 2][current.Y - 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return -1;
                            }
                        }
                    }
                    if(current.X < 6 && current.Y < 6)
                    {
                        if (cells[current.X + 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg" || cells[current.X + 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                        {
                            if (cells[current.X + 2][current.Y + 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return 1;
                            }
                        }
                    }
                }
                if(current.HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg")
                {
                    if (current.X > 1 && current.Y > 1)
                    {
                        if (cells[current.X - 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg" || cells[current.X - 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                        {
                            if (cells[current.X - 2][current.Y - 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return -1;
                            }
                        }
                    }
                    if (current.X > 1 && current.Y < 6)
                    {
                        if (cells[current.X - 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg" || cells[current.X - 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                        {
                            if (cells[current.X - 2][current.Y + 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return 1;
                            }
                        }
                    }
                    if (current.X < 6 && current.Y > 1)
                    {
                        if (cells[current.X + 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg" || cells[current.X + 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                        {
                            if (cells[current.X + 2][current.Y - 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return -1;
                            }
                        }
                    }
                    if (current.X < 6 && current.Y < 6)
                    {
                        if (cells[current.X + 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg" || cells[current.X + 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                        {
                            if (cells[current.X + 2][current.Y + 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return 1;
                            }
                        }
                    }
                }
            }
            else
            {
                if(current.HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                {
                    if (current.X < 6 && current.Y > 1)
                    {
                        if (cells[current.X + 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg" || cells[current.X + 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                        {
                            if (cells[current.X + 2][current.Y - 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return -1;
                            }
                        }
                    }
                    if (current.X < 6 && current.Y < 6)
                    {
                        if (cells[current.X + 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg" || cells[current.X + 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                        {
                            if (cells[current.X + 2][current.Y + 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return 1;
                            }
                        }
                    }
                }
                if(current.HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                {
                    if (current.X > 1 && current.Y > 1)
                    {
                        if (cells[current.X - 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg" || cells[current.X - 1][current.Y - 1].HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                        {
                            if (cells[current.X - 2][current.Y - 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return -1;
                            }
                        }
                    }
                    if (current.X > 1 && current.Y < 6)
                    {
                        if (cells[current.X - 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg" || cells[current.X - 1][current.Y + 1].HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                        {
                            if (cells[current.X - 2][current.Y + 2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            {
                                return 1;
                            }
                        }
                    }
                }
            }

            return 0;
        }

        public void ChangeTurns()
        {
            if (turn == false)
            {
                turn = true;
                GameWindow gameWindow = (Application.Current.MainWindow as GameWindow);
                gameWindow.redTurn.Text = "YOUR TURN";
                gameWindow.blackTurn.Text = "";
            }
            else
            {
                turn = false;
                GameWindow gameWindow = (Application.Current.MainWindow as GameWindow);
                gameWindow.redTurn.Text = "";
                gameWindow.blackTurn.Text = "YOUR TURN";
            }
            numberOfInvalidMoves = 0;
        }

        public void Move(Cell current, Cell newCell)
        {
            cells[current.X][current.Y].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            cells[current.X][current.Y].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            cells[newCell.X][newCell.Y].HidenImage = current.HidenImage;
            cells[newCell.X][newCell.Y].DisplayedImage = current.HidenImage;
            if(newCell.X == 0 && current.HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
            {
                newCell.DisplayedImage = "/Tema 2;component/Resources/Cells/blackKing.jpg";
                newCell.HidenImage = "/Tema 2;component/Resources/Cells/blackKing.jpg";
                cells[newCell.X][newCell.Y].HidenImage = "/Tema 2;component/Resources/Cells/blackKing.jpg";
                cells[newCell.X][newCell.Y].DisplayedImage = "/Tema 2;component/Resources/Cells/blackKing.jpg";
            }
            else
            {
                if(newCell.X == 7 && current.HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                {
                    newCell.DisplayedImage = "/Tema 2;component/Resources/Cells/redKing.jpg";
                    newCell.HidenImage = "/Tema 2;component/Resources/Cells/redKing.jpg";
                    cells[newCell.X][newCell.Y].HidenImage = "/Tema 2;component/Resources/Cells/redKing.jpg";
                    cells[newCell.X][newCell.Y].DisplayedImage = "/Tema 2;component/Resources/Cells/redKing.jpg";
                }
                else
                {
                    newCell.DisplayedImage = current.HidenImage;
                    newCell.HidenImage = current.HidenImage;
                }
            }
            current.DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            current.HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            current.IsSelected = false;
            ChangeTurns();
        }

        public void NormalMove(Cell obj)
        {
            if (obj.HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
            {
                if (Helper.CurrentCell.HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                {
                    if ((obj.X == Helper.CurrentCell.X - 1 && obj.Y == Helper.CurrentCell.Y - 1) || (obj.X == Helper.CurrentCell.X - 1 && obj.Y == Helper.CurrentCell.Y + 1))
                    {
                        Move(Helper.CurrentCell, obj);
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Invalid move!");
                        numberOfInvalidMoves++;
                        clickCounter--;
                    }
                }
                if (Helper.CurrentCell.HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                {
                    if ((obj.X == Helper.CurrentCell.X + 1 && obj.Y == Helper.CurrentCell.Y + 1) || (obj.X == Helper.CurrentCell.X + 1 && obj.Y == Helper.CurrentCell.Y - 1))
                    {
                        Move(Helper.CurrentCell, obj);
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Invalid move!");
                        numberOfInvalidMoves++;
                        clickCounter--;
                    }
                }

            }
            else
            {
                System.Windows.MessageBox.Show("Invalid move!");
                numberOfInvalidMoves++;
                clickCounter--;
            }
        }
        
        public void KingNormalMove(Cell obj)
        {
            if (obj.HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
            {
                if ((obj.X == Helper.CurrentCell.X - 1 && obj.Y == Helper.CurrentCell.Y - 1) || (obj.X == Helper.CurrentCell.X - 1 && obj.Y == Helper.CurrentCell.Y + 1) || (obj.X == Helper.CurrentCell.X + 1 && obj.Y == Helper.CurrentCell.Y - 1) || (obj.X == Helper.CurrentCell.X + 1 && obj.Y == Helper.CurrentCell.Y + 1))
                {
                    Move(Helper.CurrentCell, obj);
                }
                else
                {
                    System.Windows.MessageBox.Show("Invalid move!");
                    numberOfInvalidMoves++;
                    clickCounter--;
                }
                
            }
            else
            {
                System.Windows.MessageBox.Show("Invalid move!");
                numberOfInvalidMoves++;
                clickCounter--;
            }
        }

        public void TakeOutMove(Cell current, Cell newCell, int direction)
        {
            cells[current.X][current.Y].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            cells[current.X][current.Y].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            cells[newCell.X][newCell.Y].HidenImage = current.HidenImage;
            cells[newCell.X][newCell.Y].DisplayedImage = current.HidenImage;

            if (newCell.X == 0 && current.HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
            {
                newCell.DisplayedImage = "/Tema 2;component/Resources/Cells/blackKing.jpg";
                newCell.HidenImage = "/Tema 2;component/Resources/Cells/blackKing.jpg";
            }
            else
            {
                if (newCell.X == 7 && current.HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                {
                    newCell.DisplayedImage = "/Tema 2;component/Resources/Cells/redKing.jpg";
                    newCell.HidenImage = "/Tema 2;component/Resources/Cells/redKing.jpg";
                }
                else
                {
                    newCell.DisplayedImage = current.HidenImage;
                    newCell.HidenImage = current.HidenImage;
                }
            }
            current.DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            current.HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            current.IsSelected = false;

            if ((newCell.HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg" || newCell.HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg") && direction == -1)
            {
                cells[newCell.X + 1][newCell.Y + 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                cells[newCell.X + 1][newCell.Y + 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                Helper.DeletedCell = cells[newCell.X + 1][newCell.Y + 1];
                redPiecesErased++;
                System.Windows.MessageBox.Show("Ati eliminat o piesa rosie. Click pe ea pentru a o scoate din joc.");
                erasePiece = true;
            }
            if ((newCell.HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg" || newCell.HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg") && direction == 1)
            {

                cells[newCell.X + 1][newCell.Y - 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                cells[newCell.X + 1][newCell.Y - 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                Helper.DeletedCell = cells[newCell.X + 1][newCell.Y - 1];
                redPiecesErased++;
                System.Windows.MessageBox.Show("Ati eliminat o piesa rosie. Click pe ea pentru a o scoate din joc.");
                erasePiece = true;
            }
            if ((newCell.HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg" || newCell.HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg") && direction == -1)
            {

                cells[newCell.X - 1][newCell.Y + 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                cells[newCell.X - 1][newCell.Y + 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                Helper.DeletedCell = cells[newCell.X - 1][newCell.Y + 1];
                blackPiecesErased++;
                System.Windows.MessageBox.Show("Ati eliminat o piesa neagra. Click pe ea pentru a o scoate din joc.");
                erasePiece = true;
            }
            if ((newCell.HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg" || newCell.HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg") && direction == 1)
            {

                cells[newCell.X - 1][newCell.Y - 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                cells[newCell.X - 1][newCell.Y - 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                Helper.DeletedCell = cells[newCell.X - 1][newCell.Y - 1];
                blackPiecesErased++;
                System.Windows.MessageBox.Show("Ati eliminat o piesa neagra. Click pe ea pentru a o scoate din joc.");
                erasePiece = true;
            }

            if (CanTakeOut(newCell) != 0)
            {
                MessageBox.Show("Mai puteti elimina o piesa!");
            }
            else
            {
                if (turn == false)
                {
                    turn = true;
                    GameWindow thisWindow = (Application.Current.MainWindow as GameWindow);
                    thisWindow.redTurn.Text = "YOUR TURN";
                    thisWindow.blackTurn.Text = "";
                    if (redPiecesErased == 12)
                    {
                        Stats stats = new Stats();
                        stats.addWin(1);
                        WinWindow winWindow = new WinWindow("Black");
                        winWindow.Show();
                        GameWindow mainWindow = (Application.Current.MainWindow as GameWindow);
                        mainWindow.Close();
                        return;
                    }
                }
                else
                {
                    turn = false;
                    GameWindow thisWindow = (Application.Current.MainWindow as GameWindow);
                    thisWindow.redTurn.Text = "";
                    thisWindow.blackTurn.Text = "YOUR TURN";
                    if (blackPiecesErased == 12)
                    {
                        Stats stats = new Stats();
                        stats.addWin(2);
                        WinWindow winWindow = new WinWindow("Red");
                        winWindow.Show();
                        GameWindow mainWindow = (Application.Current.MainWindow as GameWindow);
                        mainWindow.Close();
                        return;
                    }
                }
            }

            numberOfInvalidMoves = 0;

            GameWindow gameWindow = (Application.Current.MainWindow as GameWindow);
            gameWindow.redPtsNr.Text = blackPiecesErased.ToString();
            gameWindow.blackPtsNr.Text = redPiecesErased.ToString();
        }

        public void KingTakeOutMove(Cell current, Cell newCell, int direction)
        {
            cells[current.X][current.Y].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            cells[current.X][current.Y].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            cells[newCell.X][newCell.Y].HidenImage = current.HidenImage;
            cells[newCell.X][newCell.Y].DisplayedImage = current.HidenImage; 
            newCell.DisplayedImage = current.HidenImage;
            newCell.HidenImage = current.HidenImage;
            current.DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            current.HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
            current.IsSelected = false;

            if ((newCell.HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg") && direction == -1)
            {
                if (newCell.X < current.X)
                {
                    cells[newCell.X + 1][newCell.Y + 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    cells[newCell.X + 1][newCell.Y + 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    Helper.DeletedCell = cells[newCell.X + 1][newCell.Y + 1];
                    redPiecesErased++;
                    System.Windows.MessageBox.Show("Ati scos o piesa rosie. Click pe ea pentru a o scoate din joc.");
                    erasePiece = true;
                }
                else
                {
                    cells[newCell.X - 1][newCell.Y + 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    cells[newCell.X - 1][newCell.Y + 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    Helper.DeletedCell = cells[newCell.X - 1][newCell.Y + 1];
                    blackPiecesErased++;
                    System.Windows.MessageBox.Show("Ati scos o piesa neagra. Click pe ea pentru a o scoate din joc.");
                    erasePiece = true;
                }
            }
            if ((newCell.HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg") && direction == 1)
            {
                if (newCell.X < current.X)
                {
                    cells[newCell.X + 1][newCell.Y - 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    cells[newCell.X + 1][newCell.Y - 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    Helper.DeletedCell = cells[newCell.X + 1][newCell.Y - 1];
                    redPiecesErased++;
                    System.Windows.MessageBox.Show("Ati scos o piesa rosie. Click pe ea pentru a o scoate din joc.");
                    erasePiece = true;
                }
                else
                {
                    cells[newCell.X - 1][newCell.Y - 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    cells[newCell.X - 1][newCell.Y - 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    Helper.DeletedCell = cells[newCell.X - 1][newCell.Y - 1];
                    blackPiecesErased++;
                    System.Windows.MessageBox.Show("Ati scos o piesa neagra. Click pe ea pentru a o scoate din joc.");
                    erasePiece = true;
                }
            }
            if ((newCell.HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg") && direction == -1)
            {
                if (newCell.X > current.X)
                {
                    cells[newCell.X - 1][newCell.Y + 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    cells[newCell.X - 1][newCell.Y + 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    Helper.DeletedCell = cells[newCell.X - 1][newCell.Y + 1];
                    blackPiecesErased++;
                    System.Windows.MessageBox.Show("Ati scos o piesa neagra. Click pe ea pentru a o scoate din joc.");
                    erasePiece = true;
                }
                else
                {
                    cells[newCell.X + 1][newCell.Y + 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    cells[newCell.X + 1][newCell.Y + 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    Helper.DeletedCell = cells[newCell.X + 1][newCell.Y + 1];
                    redPiecesErased++;
                    System.Windows.MessageBox.Show("Ati scos o piesa rosie. Click pe ea pentru a o scoate din joc.");
                    erasePiece = true;
                }
            }
            if ((newCell.HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg") && direction == 1)
            {
                if (newCell.X > current.X)
                {
                    cells[newCell.X - 1][newCell.Y - 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    cells[newCell.X - 1][newCell.Y - 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    Helper.DeletedCell = cells[newCell.X - 1][newCell.Y - 1];
                    blackPiecesErased++;
                    System.Windows.MessageBox.Show("Ati scos o piesa neagra. Click pe ea pentru a o scoate din joc.");
                    erasePiece = true;
                }
                else
                {
                    cells[newCell.X + 1][newCell.Y - 1].DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    cells[newCell.X + 1][newCell.Y - 1].HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                    Helper.DeletedCell = cells[newCell.X + 1][newCell.Y - 1];
                    redPiecesErased++;
                    System.Windows.MessageBox.Show("Ati scos o piesa rosie. Click pe ea pentru a o scoate din joc.");
                    erasePiece = true;
                }
            }

            if (CanTakeOut(newCell) != 0)
            {
                MessageBox.Show("Mai puteti elimina o piesa!");
            }
            else
            { 
                if (turn == false)
                {
                    turn = true;
                    GameWindow thisWindow = (Application.Current.MainWindow as GameWindow);
                    thisWindow.redTurn.Text = "YOUR TURN";
                    thisWindow.blackTurn.Text = "";
                    if (redPiecesErased == 12)
                    {
                        Stats stats = new Stats();
                        stats.addWin(1);
                        WinWindow winWindow = new WinWindow("Black");
                        winWindow.Show();
                        GameWindow mainWindow = (Application.Current.MainWindow as GameWindow);
                        mainWindow.Close();
                        return;
                    }
                }
                else
                {
                    turn = false;
                    GameWindow thisWindow = (Application.Current.MainWindow as GameWindow);
                    thisWindow.redTurn.Text = "";
                    thisWindow.blackTurn.Text = "YOUR TURN";
                    if (blackPiecesErased == 12)
                    {
                        Stats stats = new Stats();
                        stats.addWin(2);
                        WinWindow winWindow = new WinWindow("Red");
                        winWindow.Show();
                        GameWindow mainWindow = (Application.Current.MainWindow as GameWindow);
                        mainWindow.Close();
                        return;
                    }
                }
            }
        }

        public void DeletePiece(Cell obj)
        {
            if (obj.X == Helper.DeletedCell.X && obj.Y == Helper.DeletedCell.Y)
            {
                obj.HidenImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                obj.DisplayedImage = "/Tema 2;component/Resources/Cells/darkCell.jpg";
                erasePiece = false;
            }
            else
                System.Windows.MessageBox.Show("Piesa pe care ati apasat nu este cea eliminata.");
        }
        
        public void SaveConfig()
        {
            using (TextWriter tw = new StreamWriter("savedGame.txt"))
            {
                for (int index1 = 0; index1 < cells.Count; index1++)
                {
                    for (int index2 = 0; index2 < cells[index1].Count; index2++)
                    {
                        if(cells[index1][index2].HidenImage == "/Tema 2;component/Resources/Cells/lightCell.jpg")
                            tw.Write(0.ToString() + " ");
                        if (cells[index1][index2].HidenImage == "/Tema 2;component/Resources/Cells/darkCell.jpg")
                            tw.Write(1.ToString() + " ");
                        if (cells[index1][index2].HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg")
                            tw.Write(2.ToString() + " ");
                        if (cells[index1][index2].HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                            tw.Write(3.ToString() + " ");
                        if (cells[index1][index2].HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg")
                            tw.Write(4.ToString() + " ");
                        if (cells[index1][index2].HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg")
                            tw.Write(5.ToString() + " ");
                    }
                    tw.WriteLine();
                }
            }
            MessageBox.Show("Configuratia jocului a fost salvata in fisier.");
        }

        public void ClickAction(Cell obj)
        {
            if (obj.X == 7 && obj.Y == 8)
            {
                SaveConfig();
            }
            else
            {
                if (!erasePiece)
                {
                    if (numberOfInvalidMoves < 3)
                    {
                        if (turn == false && (obj.HidenImage == "/Tema 2;component/Resources/Cells/redPiece.jpg" || obj.HidenImage == "/Tema 2;component/Resources/Cells/redKing.jpg") && clickCounter % 2 == 0)
                            System.Windows.MessageBox.Show("It's player 1's turn! (BLACK)");
                        else
                            if (turn == true && (obj.HidenImage == "/Tema 2;component/Resources/Cells/blackPiece.jpg" || obj.HidenImage == "/Tema 2;component/Resources/Cells/blackKing.jpg") && clickCounter % 2 == 0)
                            System.Windows.MessageBox.Show("It's player 2's turn! (RED)");
                        else
                        {
                            clickCounter++;
                            if (clickCounter % 2 != 0)
                            {
                                SelectPiece(obj);
                            }
                            else
                            {
                                if (CanTakeOut(Helper.CurrentCell) == 1)
                                {
                                    if (Helper.CurrentCell.HidenImage != "/Tema 2;component/Resources/Cells/redKing.jpg" && Helper.CurrentCell.HidenImage != "/Tema 2;component/Resources/Cells/blackKing.jpg")
                                    {
                                        if ((obj.X == Helper.CurrentCell.X + 2 && obj.Y == Helper.CurrentCell.Y + 2) || (obj.X == Helper.CurrentCell.X - 2 && obj.Y == Helper.CurrentCell.Y + 2))
                                            TakeOutMove(Helper.CurrentCell, obj, 1);
                                        else
                                        {
                                            NormalMove(obj);
                                        }
                                    }
                                    else
                                    {
                                        if ((obj.X == Helper.CurrentCell.X + 2 && obj.Y == Helper.CurrentCell.Y + 2) || (obj.X == Helper.CurrentCell.X - 2 && obj.Y == Helper.CurrentCell.Y + 2))
                                            KingTakeOutMove(Helper.CurrentCell, obj, 1);
                                        else
                                        {
                                            KingNormalMove(obj);
                                        }
                                    }
                                }
                                else
                                {
                                    if (CanTakeOut(Helper.CurrentCell) == -1)
                                    {
                                        if (Helper.CurrentCell.HidenImage != "/Tema 2;component/Resources/Cells/redKing.jpg" && Helper.CurrentCell.HidenImage != "/Tema 2;component/Resources/Cells/blackKing.jpg")
                                        {
                                            if ((obj.X == Helper.CurrentCell.X + 2 && obj.Y == Helper.CurrentCell.Y - 2) || (obj.X == Helper.CurrentCell.X - 2 && obj.Y == Helper.CurrentCell.Y - 2))
                                                TakeOutMove(Helper.CurrentCell, obj, -1);
                                            else
                                            {
                                                NormalMove(obj);
                                            }
                                        }
                                        else
                                        {
                                            if ((obj.X == Helper.CurrentCell.X + 2 && obj.Y == Helper.CurrentCell.Y - 2) || (obj.X == Helper.CurrentCell.X - 2 && obj.Y == Helper.CurrentCell.Y - 2))
                                                KingTakeOutMove(Helper.CurrentCell, obj, -1);
                                            else
                                            {
                                                KingNormalMove(obj);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Helper.CurrentCell.HidenImage != "/Tema 2;component/Resources/Cells/redKing.jpg" && Helper.CurrentCell.HidenImage != "/Tema 2;component/Resources/Cells/blackKing.jpg")
                                            NormalMove(obj);
                                        else
                                            KingNormalMove(obj);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Stay();
                    }
                }
                else
                {
                    DeletePiece(obj);
                }

            }
        }
    }
}
