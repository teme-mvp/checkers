﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2.Models
{
    class Helper
    {
        public static Cell CurrentCell { get; set; }
        public static Cell PreviousCell { get; set; }
        public static Cell DeletedCell { get; set; }
        public static ObservableCollection<ObservableCollection<Cell>> InitGameBoard(bool fromFile)
        {
            if (!fromFile)
            {
                return new ObservableCollection<ObservableCollection<Cell>>()
                {
                new ObservableCollection<Cell>()
                {
                    new Cell(0, 0, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(0, 1, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(0, 2, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(0, 3, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(0, 4, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(0, 5, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(0, 6, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(0, 7, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg")
                },
                new ObservableCollection<Cell>()
                {
                    new Cell(1, 0, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(1, 1, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(1, 2, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(1, 3, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(1, 4, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(1, 5, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(1, 6, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(1, 7, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg")
                },
                new ObservableCollection<Cell>()
                {
                    new Cell(2, 0, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(2, 1, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(2, 2, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(2, 3, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(2, 4, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(2, 5, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg"),
                    new Cell(2, 6, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(2, 7, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/redPiece.jpg")
                },
                new ObservableCollection<Cell>()
                {
                    new Cell(3, 0, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/darkCell.jpg"),
                    new Cell(3, 1, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(3, 2, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/darkCell.jpg"),
                    new Cell(3, 3, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(3, 4, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/darkCell.jpg"),
                    new Cell(3, 5, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(3, 6, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/darkCell.jpg"),
                    new Cell(3, 7, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg")
                },
                new ObservableCollection<Cell>()
                {
                    new Cell(4, 0, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(4, 1, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/darkCell.jpg"),
                    new Cell(4, 2, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(4, 3, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/darkCell.jpg"),
                    new Cell(4, 4, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(4, 5, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/darkCell.jpg"),
                    new Cell(4, 6, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(4, 7, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/darkCell.jpg")
                },
                new ObservableCollection<Cell>()
                {
                    new Cell(5, 0, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(5, 1, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(5, 2, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(5, 3, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(5, 4, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(5, 5, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(5, 6, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(5, 7, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg")
                },
                new ObservableCollection<Cell>()
                {
                    new Cell(6, 0, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(6, 1, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(6, 2, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(6, 3, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(6, 4, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(6, 5, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(6, 6, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(6, 7, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg")
                },
                new ObservableCollection<Cell>()
                {
                    new Cell(7, 0, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(7, 1, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(7, 2, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(7, 3, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(7, 4, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(7, 5, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(7, 6, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/blackPiece.jpg"),
                    new Cell(7, 7, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Cells/lightCell.jpg"),
                    new Cell(7, 8, "/Tema 2;component/Resources/Cells/selectedPiece.jpg", "/Tema 2;component/Resources/Buttons/saveButton.png")

                }
                };
            }
            else
            {
                return new ObservableCollection<ObservableCollection<Cell>>();
            }
        }

        public static ObservableCollection<ObservableCollection<Cell>> RedrawGameBoard(ObservableCollection<ObservableCollection<Cell>> board)
        {
            return board;
        }
    }
}
