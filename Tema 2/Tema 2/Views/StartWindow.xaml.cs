﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tema_2.Models;

namespace Tema_2.Views
{
    /// <summary>
    /// Interaction logic for StartWindow.xaml
    /// </summary>
    public partial class StartWindow : Window
    {
        public StartWindow()
        {
            InitializeComponent();
        }

        private void New_Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow = new GameWindow();
            Application.Current.MainWindow.Show();
            this.Close();
        }

        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Stats_Button_Click(object sender, RoutedEventArgs e)
        {
            Stats stats = new Stats();
            stats.showStats();
        }

        private void About_Button_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow();
            aboutWindow.Show();
        }

        private void Exit_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
